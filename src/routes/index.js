import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'
import Homepage from '../container/Home/index'
class Routes extends Component {

  render() {

    return (
      <>
        <Switch>
          <Route path="/" component={Homepage} exact />
        </Switch>
      </>
    )
  }
}

export default Routes